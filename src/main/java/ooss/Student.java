package ooss;

import java.util.Optional;

public class Student extends Person {
    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
//        MessageFormat.format("{0}", xxx)
        String introduceStr = super.introduce() + " I am a student." +
                Optional.ofNullable(this.klass)
                        .map(klass -> klass.isLeader(this) ? " I am the leader of class " : " I am in class ")
                        .orElse("") +
                Optional.ofNullable(this.klass)
                        .map(Klass::getNumber)
                        .map(Object::toString)
                        .orElse("");
        introduceStr += introduceStr.endsWith(".") ? "" : ".";
        return introduceStr;
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return klass.equals(this.klass);
    }

    public Klass getKlass() {
        return klass;
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }
}
