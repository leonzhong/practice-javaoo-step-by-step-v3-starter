package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private final List<Klass> klasses;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        this.klasses = new ArrayList<>();
    }

    @Override
    public String introduce() {
        StringBuilder introduceStr = new StringBuilder();
        introduceStr.append(super.introduce()).append(" I am a teacher.");
        if (klasses.size() > 0) {
            introduceStr.append(" I teach Class")
                    .append(klasses.stream()
                            .map(klass -> " " + klass.getNumber())
                            .collect(Collectors.joining(",")))
                    .append(".");
        }
        return introduceStr.toString();
    }

    public void assignTo(Klass klass) {
        if (!klasses.contains(klass)) {
            klasses.add(klass);
        }
    }

    public boolean belongsTo(Klass klass) {
        return klasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return klasses.contains(student.getKlass());
    }
}
