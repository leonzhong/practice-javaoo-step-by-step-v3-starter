package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private final int number;
    private Student leader;
    private final List<Person> attachPerson;

    public Klass(int number) {
        this.number = number;
        this.attachPerson = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public void assignLeader(Student student) {
        if (!this.equals(student.getKlass())) {
            System.out.println("It is not one of us.");
            return;
        }
        this.leader = student;
        broadcast(this.leader);
    }

    public boolean isLeader(Student student) {
        return student.equals(this.leader);
    }

    public void attach(Person person) {
        this.attachPerson.add(person);
    }

    public void broadcast(Student leader) {
        for (Person person : this.attachPerson) {
            if (person instanceof Teacher) {
                person.announceLeader(leader.name, "teacher", this.number);
            }else if (person instanceof Student){
                person.announceLeader(leader.name, "student", this.number);
            }
        }
    }

    public int getNumber() {
        return number;
    }

}
