package ooss;

public class Person {
    protected int id;
    protected String name;
    protected int age;

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String introduce() {
        return String.format("My name is %s. I am %d years old.", this.name, this.age);
    }

    public void announceLeader(String leaderName, String identity, int className) {
        System.out.println("I am " + name + ", " + getClassDescription(identity, className) + ". I know " + leaderName + " become Leader.");
    }

    public String getClassDescription(String identity, int className) {
        return String.format("%s of Class %d", identity, className);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
