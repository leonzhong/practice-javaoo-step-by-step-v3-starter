Objective:

- Today we learned how to conduct code reviews. Initially, we exchanged code among team members and then selected one code for demonstration. This helped me learn the strengths in my colleagues' code and identify any issues in the code.
- We learned the concept of Java Stream API, and using the Stream API effectively can enhance our code.
- We learned the concept of Object-Oriented Programming (OOP) and did several exercises.

Reflective:

- I feel like I'm constantly making progress.

Interpretive:

- During code reviews, we learn from our colleagues' code, and sometimes we discover different perspectives and approaches. This helps me adjust my own thinking and overcome my weaknesses.
- Despite having a basic understanding of Stream API and OOP, I still encounter various issues when applying them practically. I feel like there's more to explore in learning the Stream API.

Decisional:

- I will approach future assignments with a diligent attitude.
- I will continue learning and using concepts like Stream API and OOP in my future development work.